import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CelularDetalhesComponent } from './celular-detalhes.component';

describe('CelularDetalhesComponent', () => {
  let component: CelularDetalhesComponent;
  let fixture: ComponentFixture<CelularDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CelularDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CelularDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
