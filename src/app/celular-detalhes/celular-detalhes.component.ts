import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {Celular} from '../celular.model';
import {CelularService} from '../celular.service';

@Component({
  selector: 'app-celular-detalhes',
  templateUrl: './celular-detalhes.component.html',
  styleUrls: ['./celular-detalhes.component.css']
})
export class CelularDetalhesComponent implements OnInit {

  constructor(private service: CelularService
            , private route: ActivatedRoute) { }

  celular: Celular;
  imagemPadrao: string;

  ngOnInit() {
    this.buscarCelular();
  }

  buscarCelular(){
    this.service.buscarCelularPorId(this.route.snapshot.params['id'])
      .subscribe(celular => this.celular = celular);
  }

}
