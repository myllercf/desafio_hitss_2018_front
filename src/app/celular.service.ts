import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers, RequestOptions} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ErrorHandler} from './app.error-handler';

import {CLARO_API} from './app.api';
import { Celular } from './celular.model';

@Injectable()
export class CelularService {

    celulares: Celular[];
    url: string = `${CLARO_API}/mobile`;
    
    constructor(private http: HttpClient){}

    listarCelulares(): Observable<Celular[]> {
        return this.http.get<Celular[]>(this.url);
    }

    buscarCelularPorId(code: string): Observable<Celular>{
        return this.http.get<Celular>(this.url+'/'+code)
    }

    cadastrarCelulares(celular: Celular): Observable<Celular> {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Access-Control-Allow-Origin': '*',
              'Access-Control-Allow-Methods': 'POST',
              'Access-Control-Allow-Headers': 'Origin, Content-Type',
            })
          };

        return this.http.post<Celular>(this.url, celular, httpOptions);
    }
}