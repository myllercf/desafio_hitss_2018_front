import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import {ROUTES} from './app.router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SobreComponent } from './sobre/sobre.component';
import { HomeComponent } from './home/home.component';
import { CelularesComponent } from './celulares/celulares.component';
import { CelularService } from './celular.service';
import { CelularDetalhesComponent } from './celular-detalhes/celular-detalhes.component';
import { CadastrarCelularComponent } from './cadastrar-celular/cadastrar-celular.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SobreComponent,
    HomeComponent,
    CelularesComponent,
    CelularDetalhesComponent,
    CadastrarCelularComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [CelularService],
  bootstrap: [AppComponent]
})
export class AppModule { }
