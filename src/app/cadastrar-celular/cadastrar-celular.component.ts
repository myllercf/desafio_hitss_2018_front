import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';

import {Celular} from '../celular.model';
import {CelularService} from '../celular.service';

@Component({
  selector: 'app-cadastrar-celular',
  templateUrl: './cadastrar-celular.component.html',
  styleUrls: ['./cadastrar-celular.component.css']
})
export class CadastrarCelularComponent implements OnInit {

  celular: Celular;
  celularForm: FormGroup;

  constructor(private service: CelularService
              , private formBuilder: FormBuilder
              , private router: Router) { }

  ngOnInit() {
    this.celularForm = this.formBuilder.group({
      model: this.formBuilder.control('', [Validators.required, Validators.minLength(2)]),
      brand: this.formBuilder.control('', [Validators.required, Validators.minLength(2)]),
      date: this.formBuilder.control('', [Validators.required]),
      price: this.formBuilder.control('', [Validators.required]),
      photo: this.formBuilder.control('', [Validators.required, Validators.minLength(2)])
    })
  }

  cadastrarNovoCelular(cel: Celular){
    this.service.cadastrarCelulares(cel);
    //alert("Cadastro Realizado com sucesso!");
    //this.router.navigate(['celulares'])
  }

}
