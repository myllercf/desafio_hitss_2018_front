import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarCelularComponent } from './cadastrar-celular.component';

describe('CadastrarCelularComponent', () => {
  let component: CadastrarCelularComponent;
  let fixture: ComponentFixture<CadastrarCelularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarCelularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarCelularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
