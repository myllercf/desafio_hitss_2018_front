class Celular {
    constructor(
        public id: number,
        public model: string,
        public price: number,
        public brand: string,
        public photo: string,
        public date: string
    ) {}
}

export {Celular}