import { Component, OnInit } from '@angular/core';

import {Celular} from '../celular.model';
import {CelularService} from '../celular.service';

@Component({
  selector: 'app-celulares',
  templateUrl: './celulares.component.html',
  styleUrls: ['./celulares.component.css']
})
export class CelularesComponent implements OnInit {

  celulares: Celular[];

  constructor(private service: CelularService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.service.listarCelulares()
      .subscribe(celulares => this.celulares = celulares);
  }

}
