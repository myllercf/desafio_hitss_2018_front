import {Routes} from '@angular/router';

import {SobreComponent} from './sobre/sobre.component';
import {HomeComponent} from './home/home.component';
import {CelularesComponent} from './celulares/celulares.component';
import {CelularDetalhesComponent} from './celular-detalhes/celular-detalhes.component';
import {CadastrarCelularComponent} from './cadastrar-celular/cadastrar-celular.component';

export const ROUTES: Routes = [
    {path: '', component: HomeComponent}
    , {path: 'sobre', component: SobreComponent}
    , {path: 'celulares', component: CelularesComponent}
    , {path: 'celulares/:id', component: CelularDetalhesComponent}
    , {path: 'cadastrar', component: CadastrarCelularComponent}
]